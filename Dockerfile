FROM node:16-alpine

WORKDIR /home/node/app

COPY package*.json ./

# Bundle app
COPY . .

CMD ["node", "index.js"]